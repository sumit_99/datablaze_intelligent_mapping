""" THIS CODE IS WRITTEN TO POPULATE THE terms_column_mapping CASSANDRA TABLE WHICH WILL STORE THE MAPPINGS OF HIVE TABLE
    COLUMNS AND LEAF TAXONOMY TERMS BASED ON NAMES, DATA TYPE, DESCRIPTION AND RULES."""


import pandas as pd
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.types import *
import uuid
import json
from pyspark.sql import Row
from pyspark.sql.functions import udf, struct
from fuzzywuzzy import fuzz
from pyspark.sql import SparkSession
import nltk, string, findspark, os
from sklearn.feature_extraction.text import TfidfVectorizer

from functools import reduce  # For Python 3.x
from pyspark.sql import DataFrame
# from Numerical_approach_extended import
import pyspark.sql.functions as functions

# import uuid


def create_spark_connection():

    findspark.init("/home/spark")

    os.environ["JAVA_HOME"] = "/home/sverma/Documents/software/jdk1.8.0_131"

    conf = SparkConf()
    conf.setMaster("local[4]")
    conf.setAppName("Spark Cassandra")
    conf.set("spark.cassandra.connection.host", "192.168.0.20")
    # conf.set("spark.cassandra.connection.port","9042")
    conf.set("spark.cassandra.auth.username", "DQube")
    conf.set("spark.cassandra.auth.password", "devops@123")

    sc = SparkContext(conf=conf)
    sql_context = SQLContext(sc)

    spark_data_frame = sql_context.read.format("org.apache.spark.sql.cassandra"). \
        options(keyspace="nlp_db", table="approved_terms_table_mapping").load()

    table_status_data_frame = sql_context.read.format("org.apache.spark.sql.cassandra"). \
        options(keyspace="testing", table="table_status_indicator_mapping").load()

    return spark_data_frame, table_status_data_frame, sql_context


class HiveMetadataProperty:

    def __init__(self, dataType, description, columnName):
        self.dataType = dataType
        self.description = description
        self.columnName = columnName

    def get_hive_metadata(self):
        print("in getter")
        print(self.description)
        return self

    def set_hive_metadata(self, dataType, description, columnName):
        self.columnName = columnName
        self.dataType = dataType
        self.description = description


# getter setter class for taxonomy term properties
class TaxonomyProperty:

    def __init__(self, dataType, description, termName):
        self.dataType = dataType
        self.description = description
        self.termName = termName

    def get_taxonomy_metadata(self):
        # print("in getter")
        # print(self.description)
        return self

    def set_taxonomy_metadata(self, dataType, description, termName):
        self.termName = termName
        self.dataType = dataType
        self.description = description


# code to match term names
# abbreviation matching code::
# column_term_match_dict = {}
def match_name(taxonomy_terms, hive_column_names):
    return float(fuzz.ratio(taxonomy_terms, hive_column_names))


# description matching code using cosine similarity

stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)


def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]


'''remove punctuation, lowercase, stem'''


def normalize(text):
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))


def cosine_sim(text1, text2):
    vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')

    tfidf = vectorizer.fit_transform([text1, text2])
    return float((tfidf * tfidf.T).A[0, 1])


def initialize_table_status_indicator_mapping(spark_data_frame, sql_context2):  # This table will initialize the table_status_indicator_mapping cassandra table
                                                                                # with all the tables populated in approved_terms_table_mapping cassandra table
                                                                                # in the table_name column and "In progress" in status column.
    list_table_status_in_progress = []

    for row in spark_data_frame:
        list_table_status_in_progress.append([row["table_name"], "In progress"])

    store_table_status(list_table_status_in_progress, sql_context2)


def create_data_type_score(column_data_type, taxonomy_data_type, terms_dictionary1):

    list_dictionary_data_type1 = []

    terms_dictionary1["mapping_metrics_type"] = 'Data Type'
    terms_dictionary1["taxonomy_metric_term"] = taxonomy_data_type
    terms_dictionary1["hive_metric_term"] = column_data_type
    terms_dictionary1["uuid"] = str(uuid.uuid4())

    if column_data_type == taxonomy_data_type:

        terms_dictionary1["mapping_match_score"] = 100.0
        terms_dictionary1["confidence_score"] = 100.0

        list_dictionary_data_type1.append(terms_dictionary1.copy())

    else:
        terms_dictionary1["mapping_match_score"] = 0.0
        terms_dictionary1["confidence_score"] = 0.0

        list_dictionary_data_type1.append(terms_dictionary1.copy())

    return list_dictionary_data_type1


def create_abbreviation_score(taxonomy_name, column_name, terms_dictionary2):

    list_dictionary_abbreviation1 = []

    score_name = match_name(taxonomy_name, column_name)

    terms_dictionary2["mapping_match_score"] = round(score_name, 0)
    terms_dictionary2["confidence_score"] = round(score_name, 0)
    terms_dictionary2["uuid"] = str(uuid.uuid4())
    terms_dictionary2["mapping_metrics_type"] = 'Name'
    terms_dictionary2["taxonomy_metric_term"] = taxonomy_name
    terms_dictionary2["hive_metric_term"] = column_name

    list_dictionary_abbreviation1.append(terms_dictionary2.copy())

    return list_dictionary_abbreviation1


def create_description_score(taxonomy_name, column_name, terms_dictionary3):

    list_dictionary_description1 = []

    score_description = cosine_sim(taxonomy_name, column_name)

    terms_dictionary3["mapping_match_score"] = round(score_description * 100, 0)
    terms_dictionary3["confidence_score"] = round(score_description * 100, 0)
    terms_dictionary3["mapping_metrics_type"] = 'Description'
    terms_dictionary3["uuid"] = str(uuid.uuid4())
    terms_dictionary3["taxonomy_metric_term"] = taxonomy_name
    terms_dictionary3["hive_metric_term"] = column_name

    list_dictionary_description1.append(terms_dictionary3.copy())

    return list_dictionary_description1


def run_mapping_approaches():

    print('\nInitiating the approaches . . .')
    spark_df1, table_status_df, sql_context1 = create_spark_connection()

    # for i in range(len(table_name_ordered_spark_df1)):
    #     print('\ni=', table_name_ordered_spark_df1[i].table_name)

    table_name_ordered_spark_df1 = spark_df1.orderBy("table_name").collect()

    initialize_table_status_indicator_mapping(table_name_ordered_spark_df1, sql_context1)

    hive_metadata_list = []
    taxonomy_metadata_list = []
    list_table_status_completed = []

    # ===========> CONTINUE FLOW

    # Iterating each row of cassandra table::
    all_rows = iter(table_name_ordered_spark_df1)

    extracted_table_name = next(all_rows).table_name  # to skip the first item in all rows.

    for row in all_rows:  # iterating on each row of terms_table_mapping cassandra table.

        if row["table_name"] == extracted_table_name:

            for taxonomyMeta in json.loads(row['leaf_term_metadata']):
                taxonomy_metadata_list.append(
                    TaxonomyProperty(taxonomyMeta['dataType'], taxonomyMeta['description'], taxonomyMeta['dname']))

                for hiveMeta in json.loads(row['table_columns_metadata']):
                    terms_dictionary = {
                                        "uuid": "", "taxonomy_metric_term": None,
                                        "hive_metric_term": "",
                                        "mapping_match_score": 0.0,
                                        "mapping_metrics_type": "",
                                        "mapping_metrics_value": "",
                                        "schema_name": "",
                                        "table_name": "",
                                        "column_name": "",
                                        "mapped_term": "",
                                        "selected_term": "",
                                        "udf": "",
                                        "data_source": "",
                                        "remarks": "",
                                        "confidence_score": 0.0
                                       }
                    terms_dictionary['schema_name'] = row['schema_name']
                    terms_dictionary['table_name'] = row['table_name']
                    terms_dictionary["mapped_term"] = taxonomyMeta['dname']
                    terms_dictionary["selected_term"] = taxonomyMeta['dname']
                    terms_dictionary["udf"] = 'udf'
                    terms_dictionary["column_name"] = hiveMeta['ColumnName']

                    terms_dictionary["uuid"] = str(uuid.uuid4())

                    hive_metadata_list.append(HiveMetadataProperty(hiveMeta['ColumnDataType'], hiveMeta['ColumnDesc'], hiveMeta['ColumnName']))

                    # CREATE DATATYPE SCORE
                    list_dictionary_data_type = create_data_type_score(str(hiveMeta['ColumnDataType']).lower(), str(taxonomyMeta['dataType']).lower(), terms_dictionary)

                    # CREATE ABBREVIATION SCORE
                    list_dictionary_abbreviation = create_abbreviation_score(str(taxonomyMeta['dname']).lower(), str(hiveMeta['ColumnName']).lower(), terms_dictionary)

                    # CREATE DESCRIPTION SCORE
                    list_dictionary_description = create_description_score(str(taxonomyMeta['description']).lower(), str(hiveMeta['ColumnDesc']).lower(), terms_dictionary)

            del terms_dictionary['taxonomy_metric_term']
            del terms_dictionary['hive_metric_term']
            store_mapping_approaches_scores(list_dictionary_abbreviation, list_dictionary_data_type, list_dictionary_description, spark_df1, sql_context1)
            list_table_status_completed.append([row["table_name"], "completed"])
            store_table_status(list_table_status_completed, sql_context1)

        else:
            extracted_table_name = row["table_name"]
    # return store_mapping_approaches_scores(list_dictionary_abbreviation, list_dictionary_data_type, list_dictionary_description, spark_df1, sql_context1)


def union_all(*dfs):  # this method merges all the data frames in to a single data frame(combined_df)
    return reduce(DataFrame.union, dfs)


def store_mapping_approaches_scores(abbreviation_list, data_type_list, description_list, spark_df_object, sql_context_object):

    print('\nStoring the mappings scores . . .')
    schema = StructType([
        StructField("uuid", StringType(), True),
        StructField("data_source", StringType(), True),
        StructField("table_name", StringType(), True),
        StructField("column_name", StringType(), True),
        StructField("mapping_metrics_type", StringType(), True),
        StructField("mapping_metrics_value", StringType(), True),
        StructField("mapped_term", StringType(), True),
        StructField("mapping_match_score", FloatType(), True),
        StructField("selected_term", StringType(), True),
        StructField("udf", StringType(), True),
        StructField("confidence_score", FloatType(), True),
        StructField("schema_name", StringType(), True),
        StructField("remarks", StringType(), True)])

    df1 = sql_context_object.createDataFrame(abbreviation_list, schema=schema)
    df2 = sql_context_object.createDataFrame(data_type_list, schema=schema)
    df3 = sql_context_object.createDataFrame(description_list, schema=schema)

    combined_df = union_all(df1, df2, df3)

    combined_df.write.format("org.apache.spark.sql.cassandra").mode('append')\
    .options(keyspace="testing", table="terms_column_mapping").save()


def store_table_status(table_status_list, sql_context_object):

    print('\nStoring the status of the tables . . .')

    schema = StructType([
        StructField("table_name", StringType(), True),
        StructField("status", StringType(), True)])

    status_df1 = sql_context_object.createDataFrame(table_status_list, schema=schema)

    status_df1.write.format("org.apache.spark.sql.cassandra").mode('append')\
    .options(keyspace="testing", table="table_status_indicator_mapping").save()


run_mapping_approaches()  # This method initiates this code.





