
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from mapping_cassandra_final import return_all_parent_terms, uuid_terms


auth_provider = PlainTextAuthProvider(
      username='DQube', password='devops@123')
cluster = Cluster(['192.168.0.20'], port=9042, auth_provider=auth_provider)
session = cluster.connect("nlp_db")


def delete_from_terms_table_mapping(schema_list, table_list):

    for table in table_list:

        select_cql_query = "select uuid from nlp_db.terms_table_mapping where schema_name='{}' and table_name='{}' allow filtering".format(schema_list[0], table)
        uuids = session.execute(select_cql_query)

        for uuid in uuids:
            delete_cql_query = "delete from nlp_db.terms_table_mapping where uuid={}".format(uuid[0])
            session.execute(delete_cql_query)

        delete_cql_query_pairing_status = "delete from nlp_db.terms_table_mapping_status where schema_name='{}' and table_name='{}'".format(schema_list[0], table)
        session.execute(delete_cql_query_pairing_status)

    return 'pairing rollback completed'


def delete_from_terms_column_mapping(schema_list, table_list):

    parent_terms = return_all_parent_terms()
    uuids = uuid_terms()
    print("UUIDS LENGTH:", len(uuids))
    for table in table_list:
        # for term in parent_terms:
            # select_cql_query = "select uuid from nlp_db.approved_terms_table_mapping where schema_name='{}' and table_name='{}' and parent_term_name='{}' allow filtering".format(schema_list[0], table, term)
            # uuids = session.execute(select_cql_query)

        for uuid1 in uuids:
            # print('\nuuid', uuid1)
            delete_cql_query = "delete from nlp_db.terms_column_mapping where uuid={} if exists".format(uuid1)
            session.execute(delete_cql_query)

        for term in parent_terms:
            delete_cql_query_mapping_status = "delete from nlp_db.terms_column_mapping_status where schema_name='{}' and table_name='{}' and parent_term_name='{}'".format(schema_list[0], table, term)
            session.execute(delete_cql_query_mapping_status)

    return 'mapping rollback completed'
