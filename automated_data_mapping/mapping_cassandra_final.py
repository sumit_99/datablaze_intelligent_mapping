""" THIS CODE IS WRITTEN TO POPULATE THE terms_column_mapping CASSANDRA TABLE WHICH WILL STORE THE MAPPINGS OF HIVE TABLE
    COLUMNS AND LEAF TAXONOMY TERMS BASED ON NAMES, DATA TYPE, DESCRIPTION AND RULES."""

from pyspark.sql.types import *
import uuid
import json
from fuzzywuzzy import fuzz
import nltk, string, findspark, os
from sklearn.feature_extraction.text import TfidfVectorizer

from functools import reduce  # For Python 3.x
from pyspark.sql import DataFrame
from spark_session_cassandra import create_spark_context, close_spark

from create_cassandra_table_items import LoadConfig


def create_spark_connection():

    load_config_object = LoadConfig()
    db_name = load_config_object.db_name

    global spark_data_frame, sql_context

    sql_context = create_spark_context()

    spark_data_frame = sql_context.read.format("org.apache.spark.sql.cassandra"). \
        options(keyspace=db_name, table="approved_terms_table_mapping").load()

    # initialize_table_status_indicator_mapping(spark_data_frame.orderBy("table_name").collect(), sql_context)

    # table_status_data_frame = sql_context.read.format("org.apache.spark.sql.cassandra"). \
    #     options(keyspace="testing", table="terms_table_mapping_status").load()

    # return spark_data_frame, sql_context


def return_all_parent_terms():
    return parent_term_names


def uuid_terms():
    return uuids


global parent_term_names
parent_term_names = []


def initialize_term_column_mapping_status(schema_list, tables_list, sql_context1, status=None):
    list_table_status_in_progress = []

    for table in tables_list:
        table_df = spark_data_frame.where(spark_data_frame.table_name == table)

        for row in table_df.collect():
            list_table_status_in_progress.append([schema_list[0], table, row['parent_term_name'], status])
            parent_term_names.append(row['parent_term_name'])

    return_all_parent_terms()

    store_mapping_status(list_table_status_in_progress, sql_context1, status)


def match_name(taxonomy_terms, hive_column_names):
    return float(fuzz.ratio(taxonomy_terms, hive_column_names))


# description matching code using cosine similarity


def stem_tokens(tokens):
    stemmer = nltk.stem.porter.PorterStemmer()
    return [stemmer.stem(item) for item in tokens]


'''remove punctuation, lowercase, stem'''


def normalize(text):
    remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))


def cosine_sim(text1, text2):
    vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')


    tfidf = vectorizer.fit_transform([text1, text2])
    return float((tfidf * tfidf.T).A[0, 1])


def create_data_type_score(column_data_type, taxonomy_data_type, terms_dictionary1):
    list_dictionary_data_type1 = []

    terms_dictionary1["mapping_metrics_type"] = 'Data Type'
    terms_dictionary1["mapping_metrics_value"] = column_data_type
    terms_dictionary1["taxonomy_metric_term"] = taxonomy_data_type
    terms_dictionary1["hive_metric_term"] = column_data_type
    terms_dictionary1["uuid"] = str(uuid.uuid4())
    uuids.append(terms_dictionary1["uuid"])
    if column_data_type == taxonomy_data_type:

        terms_dictionary1["mapping_match_score"] = 100.0
        terms_dictionary1["confidence_score"] = 100.0

        list_dictionary_data_type1.append(terms_dictionary1.copy())

    else:
        terms_dictionary1["mapping_match_score"] = 0.0
        terms_dictionary1["confidence_score"] = 0.0

        list_dictionary_data_type1.append(terms_dictionary1.copy())

    return list_dictionary_data_type1


def create_abbreviation_score(taxonomy_name, column_name, terms_dictionary2):
    list_dictionary_abbreviation1 = []

    score_name = match_name(taxonomy_name, column_name)

    terms_dictionary2["mapping_match_score"] = round(score_name, 0)
    terms_dictionary2["confidence_score"] = round(score_name, 0)
    terms_dictionary2["uuid"] = str(uuid.uuid4())
    terms_dictionary2["mapping_metrics_type"] = 'Name'
    terms_dictionary2["mapping_metrics_value"] = column_name
    terms_dictionary2["taxonomy_metric_term"] = taxonomy_name
    terms_dictionary2["hive_metric_term"] = column_name
    uuids.append(terms_dictionary2["uuid"])

    list_dictionary_abbreviation1.append(terms_dictionary2.copy())

    return list_dictionary_abbreviation1


def create_description_score(taxonomy_description, column_description, terms_dictionary3):
    # print(len(taxonomy_description), "::::::::::", len(column_description))
    list_dictionary_description1 = []
    if (taxonomy_description or column_description is None) or (len(taxonomy_description) == 0 or len(column_description) == 0):  # and (len(taxonomy_description) > 0)
        score_description = 0.0
    else:
        score_description = cosine_sim(taxonomy_description, column_description)

    terms_dictionary3["mapping_match_score"] = round(score_description * 100, 0)
    terms_dictionary3["confidence_score"] = round(score_description * 100, 0)
    terms_dictionary3["mapping_metrics_type"] = 'Description'
    terms_dictionary3["mapping_metrics_value"] = column_description
    terms_dictionary3["uuid"] = str(uuid.uuid4())
    terms_dictionary3["taxonomy_metric_term"] = taxonomy_description
    terms_dictionary3["hive_metric_term"] = column_description
    uuids.append(terms_dictionary3["uuid"])
    list_dictionary_description1.append(terms_dictionary3.copy())

    return list_dictionary_description1


global uuids
uuids = []


def run_mapping_approaches(schema_list, tables_list):
    print('\nInitiating the approaches . . .')
    # spark_df1, sql_context1 = create_spark_connection()

    initialize_term_column_mapping_status(schema_list, tables_list, sql_context, status='In Progress')

    # for i in range(len(table_name_ordered_spark_df1)):
    #     print('\ni=', table_name_ordered_spark_df1[i].table_name)

    # table_name_ordered_spark_df_list = spark_df1.orderBy("table_name").collect()

    # empty_df_columns = 'uuid, leaf_term_metadata, match_score, parent_term_name, schema_name, table_columns_metadata, table_name, taxonomy_term_id'

    # empty_df_columns = ','.join(item for item in spark_df1.columns)
    #
    # spark_df2 = sql_context1.createDataFrame([tuple('' for i in empty_df_columns.split(","))], empty_df_columns.split(",")).where("1=0")
    #
    # for table in tables_list:
    #     spark_df2 = spark_df2.union(spark_df1.where(spark_df1.table_name == table))
    #
    # spark_df2.show()
    # # initialize_table_status_indicator_mapping(table_name_ordered_spark_df_list, sql_context1)
    # print('\nspark_df2.collect() :', spark_df2.collect())
    # hive_metadata_list = []
    # taxonomy_metadata_list = []
    # list_table_status_completed = []
    #
    # # ===========> CONTINUE FLOW
    #
    # # Iterating each row of cassandra table::
    # all_rows = spark_df2.collect()
    # nr = 7
    # dr = 2

    for table in tables_list:
        table_df = spark_data_frame.where(spark_data_frame.table_name == table)

        # schema_name_query = 'use {}'.format(schema_list[0])
        # sql_context(schema_name_query)

        # load table

        # schema_query = 'use {}'.format(schema_list[0])
        # spark_session.sql(schema_query)
        # table_query = 'select * from {} limit 1'.format(table)
        # spark_session.sql(table_query)
        # print('\ndatasource_name :', table_df.columns)
        # find_data_source_query = 'select datasource_name from {} limit 1'.format()
        # data_source_type = sql_context('select')

        list_table_status_completed = []

        for row in table_df.collect():  # iterating on each row of terms_table_mapping cassandra table.
            for taxonomyMeta in json.loads(row['leaf_term_metadata']):

                for hiveMeta in json.loads(row['table_columns_metadata']):

                    terms_dictionary = {"uuid": "", "taxonomy_metric_term": None, "hive_metric_term": "",
                                        "mapping_match_score": 0.0, "mapping_metrics_type": "",
                                        "mapping_metrics_value": "", "schema_name": "", "table_name": "",
                                        "column_name": "", "mapped_term": "", "selected_term": "", "udf": "",
                                        "data_source": "", "remarks": "", "confidence_score": 0.0}

                    terms_dictionary['schema_name'] = row['schema_name']
                    terms_dictionary['table_name'] = row['table_name']
                    terms_dictionary["mapped_term"] = taxonomyMeta['dname']
                    terms_dictionary["selected_term"] = taxonomyMeta['dname']
                    terms_dictionary["udf"] = 'udf'
                    terms_dictionary["parent_term_name"] = row['parent_term_name']
                    terms_dictionary["column_name"] = hiveMeta['ColumnName']
                    terms_dictionary["data_source"] = hiveMeta['TableDataSource']

                    # terms_dictionary["uuid"] = str(uuid.uuid4())

                    # hive_metadata_list.append(HiveMetadataProperty(hiveMeta['ColumnDataType'], hiveMeta['ColumnDesc'], hiveMeta['ColumnName']))
                    # print(row['schema_name'],row["table_name"],"========+=======",row["parent_term_name"])
                    # CREATE ABBREVIATION SCORE
                    list_dictionary_abbreviation = create_abbreviation_score(str(taxonomyMeta['dname']).lower(),
                                                                             str(hiveMeta['ColumnName']).lower(),
                                                                             terms_dictionary)



                    # CREATE DESCRIPTION SCORE
                    list_dictionary_description = create_description_score(str(taxonomyMeta['description']).lower(),
                                                                           str(hiveMeta['ColumnDesc']).lower(),
                                                                           terms_dictionary)

                    # CREATE DATATYPE SCORE
                    list_dictionary_data_type = create_data_type_score(str(hiveMeta['ColumnDataType']).lower(),
                                                                       str(taxonomyMeta['dataType']).lower(),
                                                                       terms_dictionary)

                    store_mapping_approaches_scores(list_dictionary_abbreviation, list_dictionary_data_type,
                                                    list_dictionary_description, spark_data_frame, sql_context)
                    # print('table :', table, 'uuid :', terms_dictionary["uuid"])

            del terms_dictionary['taxonomy_metric_term']
            del terms_dictionary['hive_metric_term']
            # store_mapping_approaches_scores(list_dictionary_abbreviation, list_dictionary_data_type, list_dictionary_description, spark_data_frame, sql_context)
            list_table_status_completed.append([schema_list[0], table, row['parent_term_name'], "completed"])
            store_mapping_status(list_table_status_completed, sql_context)

            # print('\nCreate Error :', nr/dr)
            # dr = dr - 1
            # print("\nTERM", terms_dictionary["uuid"])
    # uuid_terms()
    print('\nlength uuids:', len(uuids))


def union_all(*dfs):  # this method merges all the data frames in to a single data frame(combined_df)
    return reduce(DataFrame.union, dfs)


def store_mapping_approaches_scores(abbreviation_list, data_type_list, description_list, spark_df_object,
                                    sql_context_object):

    load_config_object = LoadConfig()
    db_name = load_config_object.db_name

    print('\nStoring the mappings scores . . .')
    schema = StructType([
        StructField("uuid", StringType(), True),
        StructField("data_source", StringType(), True),
        StructField("table_name", StringType(), True),
        StructField("column_name", StringType(), True),
        StructField("mapping_metrics_type", StringType(), True),
        StructField("mapping_metrics_value", StringType(), True),
        StructField("mapped_term", StringType(), True),
        StructField("mapping_match_score", FloatType(), True),
        StructField("selected_term", StringType(), True),
        StructField("udf", StringType(), True),
        StructField("confidence_score", FloatType(), True),
        StructField("schema_name", StringType(), True),
        StructField("parent_term_name", StringType(), True),
        StructField("remarks", StringType(), True)])

    df1 = sql_context_object.createDataFrame(abbreviation_list, schema=schema)
    df2 = sql_context_object.createDataFrame(data_type_list, schema=schema)
    df3 = sql_context_object.createDataFrame(description_list, schema=schema)

    combined_df = union_all(df1, df2, df3)
    combined_df.write.format("org.apache.spark.sql.cassandra").mode('append') \
        .options(keyspace=db_name, table="terms_column_mapping").save()


def store_mapping_status(table_status_list, sql_context_object, status=None):

    load_config_object = LoadConfig()
    db_name = load_config_object.db_name

    print('\nStoring the status of the tables . . .')

    schema = StructType([
        StructField("schema_name", StringType(), True),
        StructField("table_name", StringType(), True),
        StructField("parent_term_name", StringType(), True),
        StructField("status", StringType(), True)])

    status_df1 = sql_context_object.createDataFrame(table_status_list, schema=schema)

    status_df1.write.format("org.apache.spark.sql.cassandra").mode('append') \
        .options(keyspace=db_name, table="terms_column_mapping_status").save()


def mapping_driver_method(schema_list, tables_list):
    create_spark_connection()

    initialize_term_column_mapping_status(schema_list, tables_list, sql_context, status='In Progress')

    run_mapping_approaches(schema_list, tables_list)


# mapping_driver_method(['pnc_db'], ['agreement'])  # This method initiates this code.
