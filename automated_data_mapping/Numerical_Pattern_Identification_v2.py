
""" THIS CODE IS WRITTEN FOR THE IDENTIFICATION OF THE NUMERICAL FORMULAS IN HIVE COLUMNS FOR AUTOMATED METADATA MAPPING """

import pandas as pd
import re
from itertools import permutations
import json
import datetime
# from Numerical_approach_extended import *


# class InitializeVariables(object):  # this class initializes all the variables and loads source data
#
#     def __init__(self):
#         self.table_name = ""
#         self.formulas1 = formulas1


# Init_Obj = InitializeVariables(None, None)


# def initialize_variables(data, formula_data):

    # with open("/home/sverma/PycharmProjects/Classification_Algorithms/AMM_Properties.config", "r") as readobject:
    #     config = json.load(readobject)
    #     dataset = config["dataset"]
    #     formula_data = config["formula_data"]
    # data = pd.read_csv(dataset)
    # formula_data = pd.read_csv(formula_data)
    # print('\ndata Shape :', data.shape)
    # return data, formula_data


def find_numeric_columns(data):
    print('\nIn find_numeric_columns()')
    # data, formula_data = initialize_variables()
    # print('\ntype(data)', type(data))
    # print('\ndata :', data)
    numerical_datatypes = ['int_', 'int8', 'int16', 'int32', 'int64', 'float_', 'float16', 'float32', 'float64']
    numerical_type_features = set()  # Creating a set named 'numerical_type_features', which will contain all numerical type column headers

    for iterator in range(len(data.columns)):
        if data.iloc[:, iterator].dtypes in numerical_datatypes:
            numerical_type_features.add(data.columns[iterator])

    print('\nNumber of numerical type columns :', len(numerical_type_features))
    return numerical_type_features  # it may contain columns whose values are like 25032012(a date format)

# In the below code we are calculating the correlation score of each column with other columns and then creating a
# dictionary in which key is a column and value is a list of those columns whose correlation with the key(column) >= 0.5


def exclude_date_columns(data):  # exclude date columns in which values are like this e.g. 25012009(25th Jan 2009)
    print('\nIn exclude_date_columns')
    # data, formula_data = initialize_variables()
    date_formats = ['%m%d%Y', '%d%m%Y', '%Y%m%d']
    numerical_type_features = find_numeric_columns(data)
    date_columns = set()

    for iterator1 in numerical_type_features:
        for date_format in date_formats:
            status = []
            for iterator3 in data[iterator1].values:

                try:
                    if datetime.datetime.strptime(str(iterator3), date_format):
                        status.append(True)
                except:
                        status.append(False)
        if status.count(True) == len(data[iterator1].values):
            date_columns.add(iterator1)

    print('date columns :\n', date_columns)
    for iterator4 in date_columns:
        numerical_type_features.remove(iterator4)

    print('\nNumerical_type_features :', numerical_type_features)
    return numerical_type_features


def calculate_correlation(data, formula_data):
    print('\nIn calculate_correlation()')
    numerical_type_features = exclude_date_columns(data)
    # data, formula_data = initialize_variables()
    column_correlation_dict = {}
    correlation_possibility_dict = {}

    for iterator1 in numerical_type_features:
        column_list = []

        for iterator2 in numerical_type_features:

            if iterator1 != iterator2:
                labels = '{} Vs. {}'.format(iterator1, iterator2)
                corr_value = float('{:.5f}'.format(data[iterator1].corr(data[iterator2])))
                column_correlation_dict[labels] = corr_value

                if corr_value >= 0.5:  # and corr_value:  # < 1.0
                    column_list.append(iterator2)
                    correlation_possibility_dict[iterator1] = column_list

    # print('\nColumns for which the correlations of other columns is > 50% \n\n')
    print('\n\n^^^^^^^^^^^^^^^^^ CORRELATION_POSSIBILITY_DICTIONARY ^^^^^^^^^^^^^^^\n\n')

    for key, value in correlation_possibility_dict.items():
        print(key, '===>', value, '\n')

    print('\nNUMBER OF TERMS for which the correlation of other terms were >= 0.5 are', len(correlation_possibility_dict.items()))
    return correlation_possibility_dict, column_correlation_dict

# print('\n^^^^^^^^^^^^^^^^^ CORRELATION_POSSIBILITY_DICTIONARY ^^^^^^^^^^^^^^^\n')
# for k, v in correlation_possibility_dict.items():
#     print(k, ":", v)


# The below dictionary contains the columns between whom the correlation has been calculated as key
# and their correlation values as value of the dictionary

def create_final_column_correlation_dict():
    print('\ncreate_final_column_correlation_dict()')
    correlation_possibility_dict, column_correlation_dict = calculate_correlation()
    final_column_correlation_dict = {}

    for key, value in column_correlation_dict.items():
        if str(value) != 'nan':
            final_column_correlation_dict[key] = value
    return final_column_correlation_dict


# The below code is written for taxonomy terms extraction for formula

def terms_extraction(formula):
    print('\nIn terms_extraction()')
    print('\nformula in terms_extraction() :', formula)
    terms = []
    print('\ntype(formula) :', type(formula[0]))
    print('\nformula :', formula)

    # converted_formula = str(formula).replace("_", "")  # replace('_', '')

    if len(formula) is not 0:
        cleaned_data = re.sub('\W+', ' ', str(formula))
        print('\ncleaned_data :', cleaned_data)
        none_list = ['', "", None]

        for iterator in cleaned_data.split():
            print('\niterator:', iterator)
            if iterator not in none_list and iterator.isalpha():
                print('\niterator jhiga lala :', iterator)
                terms.append(iterator)
                # print(Terms)
                final_terms = list(terms)
                print('\nterms :', terms)
                continue
        return terms


# below code is written for numerical formula matching.


def verify_formula(data, total_columns, formula_terms, formula):  # FORMULA = claimidentifier=5*eventidentifier+agreementidentifier
    print('\nIn verify_formula()')
    # print('\ndata :', data)
    print('\nformula_terms :', formula_terms)
    print('\ntotal_columns :', total_columns)
    column_term_match_set = set()  # set which contains the columns and the matched terms.
    formula_matched_terms_dict = {}  # Dictionary which contains the formula and the columns which satisfied the formula.

    combined_formula_terms_tuple = tuple(formula_terms)

    total_permutations = list(permutations(range(0, len(total_columns)), len(formula_terms)))
    print('\nFORMULA :', formula, type(formula))

    splitted_formula = formula.split('=')
    # print('\n  splitted:', splitted_formula,type(splitted_formula))

    for permutation in total_permutations:  # iterating for every permutation sequence out of total number of permutations
        match_indicator = []
        value_set = set()
        index_dict = {}
        permutation_length = len(permutation)

        for iterator2 in range(permutation_length):
            index_dict[formula_terms[iterator2]] = permutation[iterator2]
            # print('\nindex_dict :', index_dict)

        for iterator3 in range(len(data.iloc[:, 1])):  # this loop is used to iterate on all the values of a column at a time.
            left_val = splitted_formula[0]
            right_val = splitted_formula[1]
            check = 0
            for key, value in index_dict.items():  # in this for loop all the terms would be replaced by the values of the columns in the data.
                if check == 0:
                    splitted_formula[0] = splitted_formula[0].replace(key, str(data[total_columns[value]].values[iterator3]))
                    # print('\n splitted_formula[0] :', splitted_formula[0], type())
                    check += 1
                else:
                    splitted_formula[1] = splitted_formula[1].replace(key, str(data[total_columns[value]].values[iterator3]))
                # print('\n splitted_formula[1] :', splitted_formula, type(splitted_formula))
            try:
                if eval(splitted_formula[0]) == eval(splitted_formula[1]):
                    value_set.add(total_columns[value])
                    match_indicator.append('match')
                    # print('\nmatch_indicator :', match_indicator)
            except:
                pass

            splitted_formula[0] = left_val
            splitted_formula[1] = right_val

        if len(match_indicator) == len(data.iloc[:, 1]):
            for key, value in index_dict.items():
                column_term_match_set.add(total_columns[value])
                # formula_matched_terms_dict[formula] = column_term_match_set
                formula_matched_terms_dict[combined_formula_terms_tuple] = column_term_match_set

    # print('\ncombined_formula_terms_tuple :', combined_formula_terms_tuple)
    # print('\nformula :', formula, "::::::", 'matching columns :', column_term_match_set)
    return formula_matched_terms_dict  # This dictionary will contain formula as key and those columns which


def create_formula_terms_dict(data, formula_data):
    print('\nIn create_formula_terms_dict()')
    # data, formula_data = initialize_variables()
    formula_terms_dict = {}  # this dictionary will contain formula and taxonomy terms used in it.
    print('\nformula_data :', len(formula_data))
    for iterator1 in formula_data.values:  # Extract taxonomy terms from the formula, taking one formula as input at a time.
        print('\niterator1[0] :', iterator1[0])
        if len(iterator1) is not 0:
            formula_terms_dict[iterator1[0]] = terms_extraction(iterator1)

    print('\n\n^^^^^^^^^^^^^^^^^ FORMULA_TERMS_DICTIONARY ^^^^^^^^^^^^^^^\n\n')

    for key, value in formula_terms_dict.items():
        print(key, '===>', value, '\n')

    return formula_terms_dict
# print(len(correlation_possibility_dict))


def driver_method(data, formula_data):
    print('\nIn driver_method()')
    # data, formula_data = driver()
    # print('\nTABLE :=>', data)
    print('\nFORMULAS :=>', formula_data)

    correlation_possibility_dict, column_correlation_dict = calculate_correlation(data, formula_data)
    formula_terms_dict = create_formula_terms_dict(data, formula_data)

    for key1, value1 in correlation_possibility_dict.items():
        # print('\n', k1, ":", v1)
        for key, value in formula_terms_dict.items():
            print(len(value1))
            print(len(value))
            if len(value1) + 1 >= len(value):
                total_columns = set(value1)
                total_columns.add(key1)
                print('\nO/P of Rules approach :', verify_formula(data, list(total_columns), value, key))  # this method will be called for each
                                                                                    # key value pair of correlation_possibility_dict,
                                                                                    # and will return a dictionary which consists of
                                                                                    # formulas as key and value as those columns which
                                                                                    # satisfy the formulas.


# driver_method()  # initiates the code.



