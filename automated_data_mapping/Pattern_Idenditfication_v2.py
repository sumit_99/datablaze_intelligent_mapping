import pandas as pd
import dateutil
from dateutil.parser import parse
from datetime import datetime
# import strptime
import re, datetime, datefinder
# from pattern.en import sentiment

path = '/home/sverma/Downloads/SampleCSVInputData/Datasets/raw_csv/Data_Preparation/Consolidated_v7_copy_2.csv'

data = pd.read_csv("/home/sverma/Downloads/SampleCSVInputData/Datasets/raw_csv/Data_Preparation/Consolidated_v7_copy.csv")

# print('\nData Shape :', data.shape, '\n')

data_shape = data.shape

# Here, we are creating a  logic which will check whether the value of one column is a part of the string of any other column.
# This is done to identify if any formula or relation exists between any two or multiple columns.

delimiters = ['-', '*', '\'', '#', '$', '?', '(', ')', '[', ']', '{', '}', '@', ',', ':', '.', '=', ';', '+=', '-=', '*=', '/=', '//=', '%=', '&=', '|=', '^=', '>>=', '<<=', '**=', '!', '~', '%', '^', '+']
# print(len(delimiters))
# delimiters = ['-']
delimited_columns = set()


numerical_datatypes = ['int_', 'int8', 'int16', 'int32', 'int64', 'float_', 'float16', 'float32', 'float64']

# Creating a set named 'numerical_type_features', which will contain all numerical type column headers

alphanumeric_type_features = set()

for iterator in range(len(data.columns)):
    if data.iloc[:, iterator].dtypes not in numerical_datatypes:
        alphanumeric_type_features.add(data.columns[iterator])  # needed to separate the alphanumeric or alpha type columns
                                                                # from the completely numeric columns because checking delimiter
                                                                # in numeric type fields was not possible due to some error.

# for i in alphanumeric_type_features:
#     print(i, ":::", data[i].dtypes)
def find_delimited_columns():
    for iterator1 in alphanumeric_type_features:
        for delimiter in delimiters:
            for iterator2 in range(len(data[iterator1].values)):
                if delimiter in data[iterator1].values[iterator2]:
    #              print('\ndelimiter =', delimiter, " ::: ", data[iterator1].values[iterator2], '\n')
                   delimited_columns.add(iterator1)
    return delimited_columns


item = '2016-10-27 00:00:00'
# item.to_pydatetime()
if 'datetime.datetime' in str(type('2016-10-27 00:00:00')):  # or item.strftime("%Y"):
    print('Yes')
# else:
#     print('No')

# if item.strftime("%Y"):
#     print('Yes')

check = '2016-10-27 00:00:00'
########################################################################################################################

dt = parse('101')
datetime_datatype = type(dt)

########################################################################################################################
'''EXTRACTION OF TAXONOMY TERMS FROM FORMULAS'''

import re

string = '(aggregatelimitamount)pw2=(coveragelimittypeidentifier)pw2–4*(maximumperpersonamount)*(branchofficeidentifier)'

cleanString = re.sub('\W+', ' ', string)
# print(cleanString)

word = 'pw2'
# print(word.isalpha())

s = "I have a meeting on 2018-12-10 in New York"
match = re.search('\d{4}-\d{2}-\d{2}', s)
date = datetime.datetime.strptime(match.group(), '%Y-%m-%d').date()
# print date

var = "2005.01.15"
var2 = "O5"

a = re.match('(\d{4})[/.-](\d{2})[/.-](\d{2})$', var)

date_format = type(parse('12.31.2015'))

matches = datefinder.find_dates('12.31.2015')  # 12.31.2015

var = 'KBC!9000studios'

b = re.match('[a-z]', var)
print(b)

var1 = 'XXX-YYY-ZZZ'
var3 = 'XAAAAA001'

c1 = re.search('[a-zA-Z]', var1)
c2 = re.search('[a-zA-Z]', var3)

print('\nc1 :', c1)
print('\nc2 :', c2)

if c1 == c2:
    print('Same')
else:
    print('Different')

