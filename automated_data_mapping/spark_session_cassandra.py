
from pyspark.sql import SparkSession, SQLContext
from pyspark import SparkConf, SparkContext
from pyspark.sql.types import *
import findspark,os,json


def create_spark_session():

    findspark.init("/home/spark")
    os.environ["JAVA_HOME"] = "/home/sverma/Documents/software/jdk1.8.0_131"
    conf = SparkConf()
    conf.setMaster("local[4]")
    conf.setAppName("Spark Cassandra")
    conf.set("spark.cassandra.connection.host", "192.168.0.20")
    conf.set("spark.cassandra.connection.port", "9042")
    conf.set("spark.cassandra.auth.username", "DQube")
    conf.set("spark.cassandra.auth.password", "devops@123")
    conf.set("spark.driver.allowMultipleContexts", "true")
    spark = SparkSession.builder.master("local").appName("sparkTest").config(conf=conf).getOrCreate()
    return spark


def create_spark_context():
    spark = create_spark_session()

    global sql_context, sc
    sc = spark.sparkContext
    sql_context = SQLContext(sc)
    return sql_context


def close_spark():
    return sc.stop()

