
""" THIS FILE CONTAINS THE IMPLEMENTATION FOR IDENTIFYING THE TIMESTAMP AND DATE TYPE COLUMNS FOR AUTOMATED METADATA MAPPING"""

import datetime
from datetime_extractor import DateTimeExtractor
import json
import pandas as pd


class InitializeVariables:  # this class initializes all the variables and loads source data

    with open("/home/sverma/PycharmProjects/Classification Algorithms/AMM_Properties.config", "r") as readobject:
        config = json.load(readobject)
        dataset = config["dataset"]
    data = pd.read_csv(dataset)


Init_obj = InitializeVariables()


def identify_timestamps(value1):

    if DateTimeExtractor(str(value1)):
        return True
    else:
        return False


def identify_dates(value2):

    date_formats = ['%d/%m/%Y', '%m/%d/%Y', '%Y/%m/%d', '%B %d, %Y', '%m%d%Y', '%d%m%Y', '%Y%m%d', '%b%d%Y', '%d%b%Y', '%Y%b%d', '%d/%Y', '%Y/%d', '%d %B, %Y', '%Y, %B %d', '%b-%d-%Y', '%d-%b-%Y', '%Y-%b-%d', '%b %d, %Y',
                    '%d %b, %Y', '%Y, %b %d', '%d.%m.%Y', '%m.%d.%Y', '%Y.%m.%d', '%A, %d %B %Y', '%d %B %Y, %A', '%a, %d %B %Y', '%d %B %Y, %a']

    for date_format in date_formats:
        try:
            if datetime.datetime.strptime(str(value2), date_format):
                return True
        except:
            return False


def column_verification():
    data = Init_obj.data
    column_value_status_dict = {}
    date_timestamp_columns = set()

    for iterator1 in data.columns:
        status = []
        for iterator2 in data[iterator1].values:
            if identify_timestamps(iterator2) or identify_dates(iterator2) is True:
                status.append(True)
                column_value_status_dict[iterator1] = status
            else:
                status.append(False)
                column_value_status_dict[iterator1] = status

        for key, value in column_value_status_dict.items():
            if value.count(True) == len(data[iterator1].values):
                date_timestamp_columns.add(key)

    print('\nColumns which contains either timestamps or dates :', date_timestamp_columns)
    print('\nTotal number of timestamps and dates columns :', len(date_timestamp_columns))


column_verification()

# NOTE : DateTimeExtractor() used in this file cannot identify the following timestamp formats
# UNIDENTIFIED : {'150423 11:42:35', '11:42:35.173', '0423_11:42:35', '11:42:35', '20150423 11:42:35.173', '0423_11:42:35.883', '11:42:35,173', '23/Apr 11:42:35,173'}
