from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, SQLContext
import json
import os
from Numerical_Pattern_Identification_v2 import driver_method
import pandas as pd
# os.environ["SPARK_HOME"] = "/home/sverma/Downloads/spark-2.3.0-bin-hadoop2.7"
# os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3.6"
import requests


class InitializeData:
    with open("./AMM_Properties.config", "r") as readobject:
        config = json.load(readobject)
        taxonomy_data_txt = config["Taxonomy_data_txt"]
        schemas_url = config["schemas"]  # use "pnc_schemas" when dev server is down.
        headers = config["header"]
        schema_data = requests.get(schemas_url, headers={"Authorization": headers})
        taxonomy_terms_header = config["header"]
        taxonomy_terms_url = config["pnc_taxonomy_terms_url"]
        taxonomy_terms = requests.get(taxonomy_terms_url, headers={"Authorization": headers})
        # print('\nschema_data.json :', schema_data.json())
        taxonomy_terms_v2 = config["pnc_taxonomy_terms_offline"]


class_object = InitializeData()


def create_spark_session():
    conf = SparkConf()
    conf.setMaster("local[4]")
    conf.setAppName("Spark Cassandra")
    conf.set("spark.cassandra.connection.host", "192.168.0.20")
    conf.set("spark.cassandra.connection.port", "9042")
    conf.set("spark.cassandra.auth.username", "DQube")
    conf.set("spark.cassandra.auth.password", "devops@123")
    conf.set("hive.metastore.uris", "thrift://192.168.0.63:9043")
    spark = SparkSession.builder.master("local").appName("sparkTest").config(conf=conf).getOrCreate()
    return spark


def method1():  # to load data from cassandra as spark data frame and convert it into pandas data frame
    print('\nIn method1\n')
    spark = create_spark_session()

    df = spark.read.format("org.apache.spark.sql.cassandra").\
        options(keyspace="nlp_db", table="terms_table_mapping").load()

    # df.show()
    pandas_df1 = df.toPandas()
    # spark.stop()
    # print('\ncassandra data frame :', pandas_df1)
    return pandas_df1


def create_hive_spark_session():
    spark = SparkSession.builder.master("local").appName("sparkTest"). \
        config("hive.metastore.uris", "thrift://192.168.0.63:9083").enableHiveSupport().getOrCreate()
    print('\nTABLE HERE :', spark.sql("select * from pnc_db.agreement"))
    return spark


def extract_formulas(row1):

    formulas = []
    leaf_jsons = json.loads(row1["leaf_term_metadata"])
    # print('\nleaf_jsons :', leaf_jsons)
    for leaf_term_json in leaf_jsons:
        formulas.append(leaf_term_json["rule"])

    # print('\nFORMULAS : ', formulas)
    return formulas


def get_dnames(json_data1, dnames_set):  # this method gets all the dnames dynamically from the dev server (.63).
    for iterator in range(len(json_data1["childrens"])):
        dnames_set.add(json_data1["childrens"][iterator]["children"]["dname"])
        if json_data1["childrens"][iterator]["children"]["hasMoreChildren"] is True:
            get_dnames(json_data1["childrens"][iterator]["children"], dnames_set)

    return dnames_set


def create_dname_psuedo_dname_dict(dnames_set):   # This method creates a dictionary of dnames and psuedo names.
    dname_psuedo_dname_dict1 = {}                 # FOR EXAMPLE :
    for dname in dnames_set:                      # dname = Party Role Code
        psuedo_dname = dname.replace(" ", "")    # psuedo dname = Party_Role_Code
        dname_psuedo_dname_dict1[dname] = psuedo_dname

    # print('\n^^^^^^^^^^^^^^^ PRINTING DNAME_PSUEDO_DNAME_DICTIONARY^^^^^^^^^^^^^^^^^^^^^^^^')
    # for key, value in dname_psuedo_dname_dict1.items():
    #     print('\n', key, " : ", value)

    return dname_psuedo_dname_dict1


def replace_dname_with_psuedo_dname(formulas_list, dname_psuedo_dname_dict2):  # this method replaces dnames with psuedo dnames in each formula.
    new_formula_list = []
    # check = 0
    # for key, value in dname_psuedo_dname_dict2.items():
    #     print(key, " : ", value)

    for formula in formulas_list:
        print('formula :', formula)
        # if type(formula) is not None:

        if formula is not None and len(formula) is not 0:
            for key, value in dname_psuedo_dname_dict2.items():
                formula = formula.replace(key, value)
                formula = formula.lstrip()  # removing extra spaces from left
                formula = formula.rstrip()  # removing extra spaces from right
                # raw_formula = dname_psuedo_dname_dict2.get(formula)  # raw_formula means it(formula) may be in upper case or lower case or it may contain large number of spaces in left and/or right.
            new_formula_list.append(formula)

    return new_formula_list


def driver():
    print('\nIn driver()')
    dnames_set = set()
    spark2 = create_hive_spark_session()
    pandas_df = method1()
    # print('\npandas_df :', pandas_df)
    # taxonomy_terms = class_object.taxonomy_terms
    # taxonomy_terms_json = taxonomy_terms.json()

    taxonomy_terms_v2 = class_object.taxonomy_terms_v2  # to be used when the taxonomy service is not working.
    with open(taxonomy_terms_v2) as file:  #
        taxonomy_terms_v2_json = json.load(file)  #

    # print('\ntaxonomy_terms_v2_json :', taxonomy_terms_v2_json)  #
    dnames = get_dnames(taxonomy_terms_v2_json, dnames_set)  #

    # taxonomy_terms_json = taxonomy_terms.json()
    # print('\ntaxonomy_terms_json :', taxonomy_terms_json)

    # dnames = get_dnames(taxonomy_terms_json, dnames_set)
    # print('\nD-N-A-M-E-S :', dnames)

    dname_psuedo_dname_dict = create_dname_psuedo_dname_dict(dnames)

    for index, row in pandas_df.iterrows():
        print("\n------------------------------ START -----------------------------")
        print('\nrow= ', row)
        print('\nschema_name = ', row['schema_name'])
        schema_name_format = 'use {}'.format(row['schema_name'])

        # table_name = row['table_name']  # to be uncommented in production
        table_name = 'check_table'  # just to check whether our rules approach is working as expected.
        print('\ntable_name = ', table_name)

        spark2.sql(schema_name_format)
        # spark2.sql("select * from "+table_name).show()
        # table_names = spark2.sql('show tables')
        # table_names.show()
        # list_table_names = table_names.rdd.flatMap(lambda x: x).collect()

        # table_structure = spark2.sql('select * from {} limit 2'.format(table_name))
        # print('\nlist_table_names :', list_table_names)
        formulas1 = extract_formulas(row)
        print('\nFORMULAS LIST:', formulas1)
        # new_formulas1 = [formula for formula in formulas1 if len(formula) is not 0]

        new_formula1 = replace_dname_with_psuedo_dname(formulas1, dname_psuedo_dname_dict)
        print('\nNEW FORMULA LIST :', new_formula1)

        formula_df = pd.DataFrame(new_formula1, columns=None)  # converting list to pandas data frame
        print('\nFORMULAS DATA FRAME:', formula_df)
        print('\ntype(formula_df :', type(formula_df))
        print('\nTABLE HERE :', 'spark2.sql'+" select * from "+table_name)

        if not formula_df.empty:

            try:
                pyspark_table = spark2.sql("select * from "+table_name)
                pandas_table = pyspark_table.toPandas()  # converted the table from pyspark data frame to pandas data frame

            except :
                print('\nException : EMPTY TABLE ', table_name)
                pass

            print('\ntype(pandas_table): ', type(pandas_table))
            print('\ntype(pyspark_table): ', type(pyspark_table))
            # print('\nTABLE HERE :', pyspark_table.show())
            # init_object2 = InitializeVariables(pandas_table, formulas1)

            driver_method(pandas_table, formula_df)
            print("\n------------------------------- END ------------------------------\n")

        else:
            print('\nFormula data frame is empty')


driver()


