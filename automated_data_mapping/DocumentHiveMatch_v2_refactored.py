import os

import findspark
import nltk
import pandas as pd
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession
from sklearn.feature_extraction.text import TfidfVectorizer
import string


class InitializeVariables:

    findspark.init("/home/spark")

    os.environ["JAVA_HOME"] = "/home/sverma/Documents/software/jdk1.8.0_131"

    df = pd.read_csv('/home/sverma/Downloads/ClaimMetadata.csv')

    column_data_type = df['DataType']

    column_name = df['Attribute Name']

    column_name_list = list(column_name)

    column_description = df['Attribute Definition']

    list_column_data_type = column_data_type.tolist()

    list_column_data_type = [itr.lower() for itr in list_column_data_type]

    list_column_description = column_description.tolist()

    spark = SparkSession.builder.master("local").appName("sparkTest").config("hive.metastore.uris",
                                                                             "thrift://192.168.0.63:9083"). \
        enableHiveSupport().getOrCreate()

    spark.sql('use pnc_db').show()

    column_hive_data_type = spark.sql('desc claim_pnc').select('data_type')
    hive_column_names = spark.sql('desc claim_pnc').select('col_name')
    column_hive_data_type.show()
    column_hive_comments = spark.sql('desc claim_pnc').select('comment')
    column_hive_comments.show()

    list_hive_column_name = hive_column_names.rdd.flatMap(lambda x: x).collect()

    list_column_hive_data_type = column_hive_data_type.rdd.flatMap(lambda x: x).collect()

    list_column_hive_comments = column_hive_comments.rdd.flatMap(lambda x: x).collect()


def stem_tokens(tokens):
    stemmer = nltk.stem.porter.PorterStemmer()
    return [stemmer.stem(item) for item in tokens]


'''remove punctuation, lowercase, stem'''


def normalize(text):
    remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))


def cosine_sim(text1, text2):
    vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')
    tfidf = vectorizer.fit_transform([text1, text2])
    return (tfidf * tfidf.T).A[0, 1]


print("\n\n-----------COMPARING VALUES FROM HIVE AND CSV THROUGH NLTK DOCUMENT MATCHING CODE FOR DATA TYPES----------\n\n")

datatype_score_dict = {}
description_score_dict = {}


# The following method creates a score(Confidence Score) between taxonomy terms and metadata(hive for time being)
# columns which is the output of data type matching approach.

def create_data_type_score(dict1, dict2):
    data_type_score_dict = {}
    for key1, value1 in dict1.items():
        for key2, value2 in dict2.items():

            if key1 == key2:
                score = int(round(1/(len(dict2[key2]))*100, 2))

                for iterator1 in value1:
                    for iterator2 in value2:
                        combined_string = '{} --- {}'.format(iterator1, iterator2)
                        data_type_score_dict[combined_string] = score

    return data_type_score_dict


def data_type_matching(list_column_data_type, list_column_hive_data_type, column_name_list, list_hive_column_name):
    dict1 = {}  # creates key value pairs of data type and taxonomy terms.
    dict2 = {}  # creates key value pairs of data type and hive column names.

    for index1, csv_data_types in enumerate(list_column_data_type):
        for index2, hive_data_types in enumerate(list_column_hive_data_type):
            if hive_data_types and csv_data_types:

                num_accuracy_data_type = int(round(cosine_sim(hive_data_types, csv_data_types) * 100, 0))
                combined_string = '{} --- {}'.format(column_name_list[index1], list_hive_column_name[index2])
                datatype_score_dict[combined_string] = num_accuracy_data_type

                if num_accuracy_data_type == 100:  # 2
                    # List1.append((csv_data_types, column_name_list[index1], list_hive_column_name[index2]))
                    # List1 = sorted(list(dict.fromkeys(List1)), key=lambda x: x[0])

                    if dict1.get(csv_data_types):
                        setval1 = dict1[csv_data_types]
                        setval1.add(column_name_list[index1])

                    else:
                        set1 = set()
                        set1.add(column_name_list[index1])
                        dict1[csv_data_types] = set1

                    if dict2.get(hive_data_types):
                        setval2 = dict2[hive_data_types]
                        setval2.add(list_hive_column_name[index2])

                    else:
                        set2 = set()
                        set2.add(list_hive_column_name[index2])
                        dict2[hive_data_types] = set2

    data_type_score_dict = create_data_type_score(dict1, dict2)

    print('\n\n\n::]------- PRINTING PROBABILITY SCORE DICTIONARY --------[::')
    for k, v in data_type_score_dict.items():
        print('\n', k, ':', v)


print("\n\n\n----------COMPARING VALUES FROM HIVE AND CSV THROUGH NLTK DOCUMENT MATCHING CODE FOR DESCRIPTION/COMMENTS----------\n\n")


def description_matching(list_column_description, list_column_hive_comments, column_name_list, list_hive_column_name):
    for index1, csv_description in enumerate(list_column_description):
        for index2, hive_comments in enumerate(list_column_hive_comments):
            if hive_comments and csv_description:
                num_accuracy_description = int(round(cosine_sim(hive_comments, csv_description) * 100, 0))
                combined_string = '{} --- {}'.format(column_name_list[index1], list_hive_column_name[index2])
                description_score_dict[combined_string] = num_accuracy_description

    print('\n\n\n::]--------- PRINTING DESCRIPTION SCORE DICTIONARY ----------[::')

    for k, v in description_score_dict.items():
        print('\n', k, ":", v)

    print('\nCosine similarity between int and int64 :', cosine_sim('int', 'int64'))


def driver():  # Initiates the code
    init_obj = InitializeVariables
    column_data_type = init_obj.column_data_type
    column_name_list = init_obj.column_name_list
    list_column_data_type = init_obj.list_column_data_type
    list_column_description = init_obj.list_column_description
    list_hive_column_name = init_obj.list_hive_column_name
    list_column_hive_data_type = init_obj.list_column_hive_data_type
    list_column_hive_comments = init_obj.list_column_hive_comments

    description_matching(list_column_description, list_column_hive_comments, column_name_list, list_hive_column_name)
    data_type_matching(list_column_data_type, list_column_hive_data_type, column_name_list, list_hive_column_name)
    print('\ncolumn_data_type :', column_data_type)
    print('\nlist_column_description :', list_column_description)


driver()  # calling this method will initiate the code.
