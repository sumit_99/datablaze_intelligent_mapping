from flask import Flask
from flask import request
from requests import get
import sys
import socket
from create_cassandra_table_items import driver_method
from delete_from_cassandra import delete_from_terms_table_mapping
from pyspark import SparkConf
from delete_from_cassandra import delete_from_terms_column_mapping
from mapping_cassandra_final import mapping_driver_method
from spark_session_cassandra import create_spark_session
from pyspark.sql import SparkSession


app = Flask(__name__)
 # 192.168.0.63:9083


@app.route('/AMP')
def pairing():
    schema_tables1 = request.args.get('table_names')

    schema_tables_list1 = schema_tables1.split(',')

    schema_list = [schema_tables_list1[0]]
    # print('\nSCHEMA :', schema_list)

    schema_tables_list1.remove(schema_tables_list1[0])

    tables_list = schema_tables_list1
    # print('\nTABLE(S) :', tables_list)

    # host_name = socket.gethostname()
    # host_ip = socket.gethostbyname(host_name)
    # print("Hostname :  ", host_name)
    # print("IP : ", host_ip)

    print('IP ADDRESS :', socket.gethostbyname(socket.gethostname()))

    # driver_method(schema_list, tables_list)

    try:
        # spark = connect_to_hive()
        # spark.sql("show databases").show()
        driver_method(schema_list, tables_list)

    except:
        # print('\n\nPROCESS INTERRUPTED : ', sys.exc_info()[0])
        # print('ERROR INFO', sys.exc_info())
        raise
        # The below method will initiate the rollback mechanism i.e if the user has selected 3 tables and the processing fails
        # due to any reason for 3rd table, then this method will delete all the data of previous 2 tables from
        # terms_table_mapping and will also delete the 'completed' statuses of those from terms_table_mapping_status table.
        #
        # pairing_rollback_response = delete_from_terms_table_mapping(schema_list, tables_list)
        # print('\nrollback_response :', rollback_response)
        # return pairing_rollback_response
    #
    return '<p>pairing completed successfully</p>'


@app.route('/AMM')
def mapping():

    schema_tables = request.args.get('table_names')
    print('\nschema_list :', schema_tables)

    schema_tables_list = schema_tables.split(',')
    print('\nschema_tables_list :', schema_tables_list)

    schema_list = [schema_tables_list[0]]
    print('\nschema_list :', schema_list)

    schema_tables_list.remove(schema_tables_list[0])

    tables_list = schema_tables_list
    print('\ntables_list :', tables_list)

    try:
        # global spark
        # spark = connect_to_hive()
        # run_mapping_approaches(schema_list, tables_list)
        mapping_driver_method(schema_list, tables_list)

    except:
        raise
        # mapping_rollback_response = delete_from_terms_column_mapping(schema_list, tables_list)
        # return mapping_rollback_response

    return '<p>mapping completed successfully</p>'


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=5000, debug=True)
