import json
import requests

taxonomy_details = []
final_taxonomy_details = []


def create_child_json(taxonomy_details1):

    taxonomy_term_details = []
    term_names = []
    taxonomy_details2 = []  # this will contain unique lists based on their dnames.

    for index, iterator in enumerate(taxonomy_details1):
        if iterator[0] not in term_names and iterator[7] is not 0:
            term_names.append(iterator[0])
            taxonomy_details2.append(iterator)

    child_dnames = []

    for iterator1 in taxonomy_details2:

        child_list = []

        for index, iterator2 in enumerate(iterator1[8]):
            child_json = {
                          "id": iterator2[7],
                          "dname": iterator2[0],
                          "numberOfChilds": iterator2[6],
                          "sdmMapping": iterator2[1],
                          "dataType": iterator2[3],
                          "abbreviation": iterator2[4],
                          "description": iterator2[5],
                          "rule": iterator2[2]
                         }
            child_json_transformed = json.dumps(child_json)
            # print('\n child_json_transformed :', child_json_transformed)

            # if iterator2[0] not in child_dnames:  # if is needed to prevent the duplicacy of those leaf terms who have multiple parents
            child_list.append(child_json_transformed)  # because those are getting repeated on the Mapping UI screen.
            child_dnames.append(iterator2[0])
                # print('\n child_dnames :', child_dnames)

            # print('iterator1 :', iterator1[0], ', child_list :', child_list)
        taxonomy_term_details.append([iterator1[0], len(iterator1[8]), child_list, iterator1[6]])

    # for i in taxonomy_term_details:
    #     print('\ni :', i)
    # print('\n child_json_transformed :', type(child_json_transformed))
    # print('\nlen(child_dnames) :', len(child_dnames), ', len(set(child_dnames)) :', len(set(child_dnames)))
    return taxonomy_term_details


def parse_json(recursive_json):

    for iterator1 in range(len(recursive_json["childrens"])):  # This for loop will maintain the parent info.
        dname = recursive_json["childrens"][iterator1]["children"]["dname"]
        sdm_mapping = recursive_json["childrens"][iterator1]["children"]["sdmMapping"]
        rule = recursive_json["childrens"][iterator1]["children"]["rule"]
        data_type = recursive_json["childrens"][iterator1]["children"]["type"]
        abbr = recursive_json["childrens"][iterator1]["children"]["abbreviation"]
        desc = recursive_json["childrens"][iterator1]["children"]["description"]
        term_id = recursive_json["childrens"][iterator1]["children"]["id"]

        if recursive_json["childrens"][iterator1]["children"]["hasMoreChildren"] is True:
            number_of_childs = len(recursive_json["childrens"][iterator1]["children"]["childrens"])
            term_details = [dname, sdm_mapping, rule, data_type, abbr, desc, term_id]
            child_list = []

            for iterator3 in range(number_of_childs):  # This for loop will maintain the child info.
                child_dname = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["dname"]
                child_sdm_mapping = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["sdmMapping"]
                child_rule = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["rule"]
                child_data_type = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["type"]
                child_abbrv = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["abbreviation"]
                child_desc = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["description"]
                child_term_id = recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["id"]

                if recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"]["hasMoreChildren"] is False:
                    child_details = [child_dname, child_sdm_mapping, child_rule, child_data_type, child_abbrv, child_desc, 0, child_term_id]
                    child_list.append(child_details)
                else:
                    parse_json(recursive_json["childrens"][iterator1]["children"]["childrens"][iterator3]["children"])

            term_details.append(len(child_list))
            term_details.append(child_list)
            taxonomy_details.append(term_details)

        else:
            parse_json(recursive_json["childrens"][iterator1]["children"])

    return taxonomy_details


with open("./AMM_Properties.config", "r") as readobject:
    config = json.load(readobject)
    taxonomy_data_txt = config["Taxonomy_data_txt"]
    schemas_url = config["schemas"]  # use "pnc_schemas" when dev server is down.
    headers = config["header"]
    # schema_data = requests.get(schemas_url, headers={"Authorization": headers})
    taxonomy_terms_header = config["header"]
    taxonomy_terms_url = config["pnc_taxonomy_terms_url"]
    # taxonomy_terms = requests.get(taxonomy_terms_url, headers={"Authorization": headers})
    # taxonomy_terms_json = taxonomy_terms.json()
    # print(taxonomy_terms_json)
    # parse_json(taxonomy_terms_json)
    # with open("/home/sverma/Desktop/pnc_json_data.txt") as readobject2:
    #     pnc_taxonomy_json = json.load(readobject2)
    #     taxonomy_details_v2 = parse_json(pnc_taxonomy_json)
        # for a in taxonomy_details_v2:
        # print("print a hi krna:",a)
        # create_child_json(taxonomy_details_v2)


