""" THIS CODE IS WRITTEN TO CREATE THE PARENT TERM AND HIVE TABLE PAIRS TO REDUCE THE NUMBER OF ITERATIONS
    AND THE AUTOMATED METADATA MAPPING APPROACHES WILL RUN FOR THESE PAIRS ONLY. THE RESULT OF THIS TABLE
    WILL BE STORED IN terms_table_mapping CASSANDRA TABLE"""

import os
import requests
import findspark
import uuid

from pyspark.sql import SparkSession, SQLContext
from pyspark.sql.types import *
import sys
import warnings
import json
from parse_taxonomy_v2 import parse_json
from parse_taxonomy_v2 import create_child_json
from spark_session_cassandra import create_spark_context, close_spark

if not sys.warnoptions:
    warnings.simplefilter("ignore")

findspark.init("/home/spark")

os.environ["JAVA_HOME"] = "/home/sverma/Documents/software/jdk1.8.0_131"


class LoadConfig:
    with open("./AMM_Properties.config", "r") as read_object:
        config = json.load(read_object)
        taxonomy_data_txt = config["Taxonomy_data_txt"]
        schemas_url = config["schemas"]  # use "pnc_schemas" when dev server is down.
        headers = config["header"]
        # schema_data = requests.get(schemas_url, headers={"Authorization": headers})  # to be commented when dev is down.
        taxonomy_terms_header = config["header"]
        taxonomy_terms_url = config["pnc_taxonomy_terms_url"]  # to be commented when dev is down.
        taxonomy_terms = requests.get(taxonomy_terms_url,
                                      headers={"Authorization": headers})  # to be commented when dev is down.
        # print('\nschema_data.json :', schema_data.json())                                      # to be commented when dev is down.
        taxonomy_terms_v2 = config["pnc_taxonomy_terms_offline"]
        # config = json.load(read_object)
        db_name = config["database_name"]
        hive_ip_and_port = config["hive_ip_port"]


def initialize_term_table_mapping_status(schema_list1, tables_list1, sql_context3,
                                         status=None):  # This table will initialize the table_status_indicator_mapping cassandra table
    # with all the tables populated in approved_terms_table_mapping cassandra table
    # in the table_name column and "In progress" in status column.
    list_table_status_in_progress = []

    for table in tables_list1:
        list_table_status_in_progress.append([schema_list1[0], table, status])

    store_table_status(list_table_status_in_progress, sql_context3, status)


def store_table_status(table_status_list, sql_context_object, status):

    load_config_object = LoadConfig()

    db_name = load_config_object.db_name

    print('\nStoring the status of the tables in MODE [ %s ]' % status)

    schema = StructType([
        StructField("schema_name", StringType(), True),

        StructField("table_name", StringType(), True),
        StructField("status", StringType(), True)])

    status_df1 = sql_context_object.createDataFrame(table_status_list, schema=schema)

    status_df1.write.format("org.apache.spark.sql.cassandra").mode('append') \
        .options(keyspace=db_name,
                 table="terms_table_mapping_status").save()


def get_schemas():
    load_config_obj = LoadConfig()
    schema_data = load_config_obj.schema_data
    schema_data_json = schema_data.json()
    schemas_list = schema_data_json["data"]

    # with open(schemas) as file:
    #     schema_data = json.load(file)

    # schemas_list = schema_data['data']
    print('\nschemas_list :', sorted(schemas_list))

    return schemas_list


""" The following methods returns hive details in the following format :-
        [0] : schema name, [1] : hive table name, [2] : no. of columns in table, [3] : column name, 
        [4] : column data type, [5] : column description/comments """


def create_hive_tables_hive_columns_details(schema_list, tables_list, spark1):
    # print('schema_list :', schema_list, 'tables_list :', tables_list)

    load_config_obj1 = LoadConfig()
    taxonomy_terms = load_config_obj1.taxonomy_terms  # to be used to get the json dynamically from dev server.
    taxonomy_terms_json = taxonomy_terms.json()  # to be used to get the json dynamically from dev server.
    taxonomy_terms_v2 = load_config_obj1.taxonomy_terms_v2  # to be used when dev server is down or swagger taxonomy_tree service is producing BAD REQUEST error.

    with open(taxonomy_terms_v2) as f:
        taxonomy_terms_v2_json = json.load(f)

    # create_spark_context()

    # spark1 = connect_to_hive()

    for schema in schema_list:
        schema_name = 'use {}'.format(schema)
        spark1.sql(schema_name)

        for hive_table in tables_list:

            hive_column_details1 = []
            json_list = []

            table_name = 'desc {}'.format(hive_table)
            hive_column_names = spark1.sql(table_name).select('col_name')
            list_hive_column_name = hive_column_names.rdd.flatMap(lambda x: x).collect()

            hive_column_comments = spark1.sql(table_name).select('comment')
            list_hive_column_comments = hive_column_comments.rdd.flatMap(lambda x: x).collect()

            hive_column_data_types = spark1.sql(table_name).select('data_type')
            list_hive_column_data_types = hive_column_data_types.rdd.flatMap(lambda x: x).collect()

            try:
                datasource_name = spark1.sql("select * from " + schema + "."
                                             + hive_table + " limit 1").select(
                    "datasource_name").first()[0]

            except:
                datasource_name = None
            # print("datasource ", datasource_name)

            for column_index in range(0, len(
                    list_hive_column_name)):  # This loop will give all the columns(data of a single column would be produced as a json object), of a single table in a single list.
                # print("[" + list_hive_column_name[column_index] + "]")

                if list_hive_column_name[column_index] and list_hive_column_comments[column_index] is not None:
                    list_hive_column_name_without_quotes = list_hive_column_name[column_index].replace('"', "")
                    list_hive_column_comments_without_quotes = list_hive_column_comments[column_index].replace('"', "")

                    json_object = {
                        "ColumnName": list_hive_column_name_without_quotes,  # list_hive_column_name[column_index],
                        "ColumnDataType": list_hive_column_data_types[column_index],
                        "ColumnDesc": list_hive_column_comments_without_quotes,
                        "TableDataSource": datasource_name
                    }

                else:
                    json_object = {
                        "ColumnName": list_hive_column_name[column_index],  # list_hive_column_name[column_index],
                        "ColumnDataType": list_hive_column_data_types[column_index],
                        "ColumnDesc": list_hive_column_comments[column_index],
                        "TableDataSource": datasource_name
                    }

                json_object_transformed = json.dumps(json_object)
                print('\n json_object_transformed :', json_object_transformed)

                json_list.append(json_object_transformed)
                # print('\nSIZE of json_list :', len(json_list))

            taxonomy_details = parse_json(taxonomy_terms_v2_json)
            taxonomy_term_details = create_child_json(taxonomy_details)

            hive_column_details1.append([schema, hive_table, len(list_hive_column_name), json_list])

            term_table_list3 = create_taxonomy_term_hive_table_pairs(hive_column_details1, taxonomy_term_details)

            store_results(term_table_list3)

            initialize_term_table_mapping_status(schema_list, [hive_table], sql_context, status="completed")

    # return hive_column_details


def read_json(json_data, name, traversal_path):
    # print("In ReadJSON2")
    for iterator in json_data:
        iterator1 = iterator["children"]
        name.add(iterator1["name"])  # Name.append(iterator1["name"])
        traversal_path.append(iterator1["traversalPath"])
        if iterator["children"]["childrens"] is not []:
            read_json(iterator["children"]["childrens"], name, traversal_path)
    return name, traversal_path


"""############ CREATING SPARK SESSION ########################################"""


#
#
# def create_spark_session():
#     conf = SparkConf()
#     conf.setMaster("local[4]")
#     conf.setAppName("Spark Cassandra")
#     conf.set("spark.cassandra.connection.host", "192.168.0.20")
#     conf.set("spark.cassandra.connection.port", "9042")
#     conf.set("spark.cassandra.auth.username", "DQube")
#     conf.set("spark.cassandra.auth.password", "devops@123")
#     conf.set("spark.driver.allowMultipleContexts", "true")
#     spark = SparkSession.builder.master("local").appName("sparkTest").config(conf=conf).getOrCreate()
#     return spark
#
#
# def create_spark_context():
#     spark = create_spark_session()
#     sc = spark.sparkContext
#
#     global sql_context
#
#     sql_context = SQLContext(sc)

# return spark, sc, sql_context


def store_results(term_table_list):

    load_config_object = LoadConfig()
    db_name = load_config_object.db_name

    schema = StructType([
        StructField("uuid", StringType(), True),
        StructField("approved", BooleanType(), True),
        StructField("leaf_term_metadata", StringType(), True),
        StructField("match_score", IntegerType(), True),
        StructField("parent_term_name", StringType(), True),
        StructField("schema_name", StringType(), True),
        StructField("table_columns_metadata", StringType(), True),
        StructField("table_name", StringType(), True),
        StructField("taxonomy_term_id", IntegerType(), True)])

    # spark1, sc1, sql_context1 = create_spark_context()

    spark_df = sql_context.createDataFrame(term_table_list, schema=schema)
    print("\nSTORED DATA PREVIEW")
    spark_df.show()

    spark_df.write.format("org.apache.spark.sql.cassandra").mode('append'). \
        options(keyspace=db_name, table="terms_table_mapping").save()


#############################################################################


""" schema, hive_table, len(list_hive_column_name), json_list2 
    column_detail[0] : schema, column_detail[1] : hive table name, column_detail[2] : number of columns in table
    column_detail[3] : columns json
"""

""" [iterator1[0], len(iterator1[7]), child_list])
    term_detail[0] : parent taxonomy term name, term_detail[1] : number of leaf terms, term_detail[2] : leaf terms json
    term_detail[3] : parent taxonomy term id
"""


# terms_table_mapping structure
# uuid|leaf_term_metadata|match_score|parent_term_name|schema_name|table_columns_metadata|table_name|taxonomy_term_id|


def create_taxonomy_term_hive_table_pairs(hive_column_details, taxonomy_term_details):
    term_table_list = []
    # hive_schema_name, hive_table_name, num_hive_columns, hive_column_json, term_name, num_leaf_terms, leaf_term_json, scores = [], [], [], [], [], [], [], [], []

    for column_detail in hive_column_details:
        for term_detail in taxonomy_term_details:

            if column_detail[2] >= term_detail[1]:
                score = int(round((term_detail[1] / column_detail[2]) * 100, 0))
                if score is not 0:  # avoiding to append those pairs whose score is 0.
                    term_table_list.append([term_detail[2], score, term_detail[0], column_detail[0], column_detail[3],
                                            column_detail[1], term_detail[3]])

            elif column_detail[2] < term_detail[1]:
                score = int(round((column_detail[2] / term_detail[1]) * 100, 0))
                if score is not 0:  # avoiding to append those pairs whose score is 0.
                    term_table_list.append([term_detail[2], score, term_detail[0], column_detail[0], column_detail[3],
                                            column_detail[1], term_detail[3]])

    for iterator2 in term_table_list:
        uuid_four = uuid.uuid4()
        iterator2.insert(0, str(uuid_four))  # inserting uuid at [0] index of term_table_list
        iterator2.insert(1, False)  # inserting approved column value(False) at [1] index of term_table_list
        # print('\niterator2 :', iterator2)

    print('\nsize of term_table_list :', len(term_table_list))
    return term_table_list


def driver_method(schema_list, tables_list):
    print('\nDRIVER METHOD STARTED . . . ')
    spark = connect_to_hive()
    global sql_context
    sql_context = create_spark_context()
    # The below called method will put the table names specified by the user in term_table_mapping_status cassandra table and status
    # will be indicated as "In progress"

    initialize_term_table_mapping_status(schema_list, tables_list, sql_context, status='In_progress')

    create_hive_tables_hive_columns_details(schema_list, tables_list, spark)

    # close_spark()


def connect_to_hive():

    load_config_object = LoadConfig()
    hive_ip_port = load_config_object.hive_ip_and_port

    # create_spark_context()
    spark = SparkSession.builder.master("local").appName("sparkTest"). \
        config("hive.metastore.uris",
               "thrift://"+hive_ip_port).enableHiveSupport().getOrCreate()  # 192.168.0.63:9083

    return spark

# driver_method(['as_noschema'],["check_table"], connect_to_hive())
